$fn=60;

length=70;
width=25;
height=20;
thickness=4;
inner=25;           // Distance(mm) inside the bracket
innerTop=22;
holeDistance=length-19;
holeRadius=4;
endRadius=4;
innerRadiusBottom=thickness/2;
outerRadiusBottom=thickness/2;
innerRadiusTop=thickness/2;
outerRadiusTop=thickness/2;

// TODO: holes

module halveBracket() {
    union() {
        hull()
        {
            translate( [-width/2+endRadius,-length/2+endRadius,0] )
                cylinder( r=endRadius, h=thickness );
            translate( [ width/2-endRadius,-length/2+endRadius,0] )
                cylinder( r=endRadius, h=thickness );
            //difference()
            translate( [-width/2,-inner/2-innerRadiusBottom,innerRadiusBottom] )
            union()
            {
                translate( [0,-innerRadiusBottom,0] )
                    cube( [width,innerRadiusBottom*2,thickness-innerRadiusBottom] );
                difference()
                {
                    rotate( a=90, v=[0,1,0] )
                        cylinder( r=innerRadiusBottom, h=width );
                    translate( [0,-innerRadiusBottom,0] )
                        cube( [width,innerRadiusBottom*2,innerRadiusBottom] );
                }
            }
        }

        hull()
        {
            translate( [-width/2,-inner/2-innerRadiusBottom,innerRadiusBottom] )         // bottom
                rotate( a=90, v=[0,1,0] )
                    cylinder( r=innerRadiusBottom, h=width );
            translate( [-width/2,-innerTop/2-innerRadiusTop,height-innerRadiusTop] )     // top
                rotate( a=90, v=[0,1,0] )
                    cylinder( r=innerRadiusTop, h=width );
        }

        hull()
        {
            translate( [-width/2,-innerTop/2-innerRadiusTop,height-innerRadiusTop] )     // top (corner)
                rotate( a=90, v=[0,1,0] )
                    cylinder( r=innerRadiusTop, h=width );
            
            translate( [-width/2,-1,height-thickness] )         // middle
                cube( [width,1,thickness] );
        }
    }
}


difference() {
    union() {
        halveBracket();
        rotate( a=180, v=[0,0,1] )
            halveBracket();
    }
    union() {
        translate( [0,holeDistance/2,0] )
            cylinder( r=holeRadius, h=thickness );
        translate( [0,-holeDistance/2,0] )
            cylinder( r=holeRadius, h=thickness );
    }
}